/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-eai',
      type: 'EAI',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const EAI = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] EAI Adapter Test', () => {
  describe('EAI Class Tests', () => {
    const a = new EAI(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('eai'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('eai'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('EAI', pronghornDotJson.export);
          assert.equal('EAI', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-eai', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('eai'));
          assert.equal('EAI', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-eai', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-eai', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */
    // define data used throughout the test
    const fakeString = 'foobar';
    const fakeNumber = 123;
    const fakeObject = { fakeString, fakeNumber };

    describe('#getCards - errors', () => {
      it('should have a getCards function', (done) => {
        try {
          assert.equal(true, typeof a.getCards === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getCardTemplates - errors', () => {
      it('should have a getCardTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getCardTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#createCard - errors', () => {
      it('should have a createCard function', (done) => {
        try {
          assert.equal(true, typeof a.createCard === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should createCard - error no data', (done) => {
        try {
          a.createCard(null, null, null, (data, error) => {
            try {
              const displayE = 'cardData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-createCard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createCard - no name', (done) => {
        try {
          const cardObj = {
            type: 'oci/card',
            cardtype: 'CardType1',
            status: 'Ordered',
            slotOccupancy: 1,
            parentSlots: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/slot',
                  key: {
                    type: 'oci/slotKey',
                    keyValue: 70
                  }
                }
              }
            ]
          };
          a.createCard(cardObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createCard - no type', (done) => {
        try {
          const cardObj = {
            name: fakeString,
            cardtype: 'CardType1',
            status: 'Ordered',
            slotOccupancy: 1,
            parentSlots: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/slot',
                  key: {
                    type: 'oci/slotKey',
                    keyValue: 70
                  }
                }
              }
            ]
          };
          a.createCard(cardObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createCard - no card type', (done) => {
        try {
          const cardObj = {
            name: fakeString,
            type: 'oci/card',
            status: 'Ordered',
            slotOccupancy: 1,
            parentSlots: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/slot',
                  key: {
                    type: 'oci/slotKey',
                    keyValue: 70
                  }
                }
              }
            ]
          };
          a.createCard(cardObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'cardtype\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createCard - no status', (done) => {
        try {
          const cardObj = {
            name: fakeString,
            type: 'oci/card',
            cardtype: 'CardType1',
            slotOccupancy: 1,
            parentSlots: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/slot',
                  key: {
                    type: 'oci/slotKey',
                    keyValue: 70
                  }
                }
              }
            ]
          };
          a.createCard(cardObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'status\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createCard - no slot occupancy', (done) => {
        try {
          const cardObj = {
            name: fakeString,
            type: 'oci/card',
            cardtype: 'CardType1',
            status: 'Ordered',
            parentSlots: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/slot',
                  key: {
                    type: 'oci/slotKey',
                    keyValue: 70
                  }
                }
              }
            ]
          };
          a.createCard(cardObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'slotOccupancy\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createCard - no parent slots', (done) => {
        try {
          const cardObj = {
            name: fakeString,
            type: 'oci/card',
            cardtype: 'CardType1',
            status: 'Ordered',
            slotOccupancy: 1
          };
          a.createCard(cardObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'parentSlots\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCardTemplate - errors', () => {
      it('should have a createCardTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createCardTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should createCardTemplate - error no data', (done) => {
        try {
          a.createCardTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'templateData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-createCardTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createCardTemplate - no name', (done) => {
        try {
          const templateObj = {
            type: 'oci/cardtemplate',
            cardTemplateType: 'CardType1',
            status: 'Ordered',
            slotOccupancy: 1
          };
          a.createCardTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createCardTemplate - no type', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            cardTemplateType: 'CardType1',
            status: 'Ordered',
            slotOccupancy: 1
          };
          a.createCardTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createCardTemplate - no card template type', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            type: 'oci/cardtemplate',
            status: 'Ordered',
            slotOccupancy: 1
          };
          a.createCardTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'cardTemplateType\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createCardTemplate - no status', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            type: 'oci/cardtemplate',
            cardTemplateType: 'CardType1',
            slotOccupancy: 1
          };
          a.createCardTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'status\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createCardTemplate - no slot occupancy', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            type: 'oci/cardtemplate',
            cardTemplateType: 'CardType1',
            status: 'Ordered'
          };
          a.createCardTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'slotOccupancy\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCard - errors', () => {
      it('should have a updateCard function', (done) => {
        try {
          assert.equal(true, typeof a.updateCard === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should updateCard - error no id', (done) => {
        try {
          a.updateCard(null, null, null, null, (data, error) => {
            try {
              const displayE = 'cardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateCard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateCard - error no data', (done) => {
        try {
          a.updateCard('123', null, null, null, (data, error) => {
            try {
              const displayE = 'cardData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateCard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateCard - no type in data', (done) => {
        try {
          a.updateCard('123', fakeObject, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCardTemplate - errors', () => {
      it('should have a updateCardTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.updateCardTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should updateCardTemplate - error no id', (done) => {
        try {
          a.updateCardTemplate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateCardTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateCardTemplate - error no data', (done) => {
        try {
          a.updateCardTemplate('123', null, null, null, (data, error) => {
            try {
              const displayE = 'templateData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateCardTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateCardTemplate - no type in data', (done) => {
        try {
          a.updateCardTemplate('123', fakeObject, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCard - errors', () => {
      it('should have a deleteCard function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCard === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should deleteCard - no id', (done) => {
        try {
          a.deleteCard(null, null, null, (data, error) => {
            try {
              const displayE = 'cardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-deleteCard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCardTemplate - errors', () => {
      it('should have a deleteCardTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCardTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should deleteCardTemplate - no id', (done) => {
        try {
          a.deleteCardTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-deleteCardTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContainers - errors', () => {
      it('should have a getContainers function', (done) => {
        try {
          assert.equal(true, typeof a.getContainers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getContainerTemplates - errors', () => {
      it('should have a getContainerTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getContainerTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#createContainer - errors', () => {
      it('should have a createContainer function', (done) => {
        try {
          assert.equal(true, typeof a.createContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should createContainer - no data', (done) => {
        try {
          a.createContainer(null, null, null, (data, error) => {
            try {
              const displayE = 'containerData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-createContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createContainer - no name', (done) => {
        try {
          const containerObj = {
            type: 'oci/container',
            containertype: 'SERVICE',
            status: 'PENDING',
            vendor: 'SIEMENS',
            parentSite: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/site',
                  key: {
                    type: 'oci/siteKey',
                    keyValue: 34
                  }
                }
              }
            ]
          };
          a.createContainer(containerObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createContainer - no type', (done) => {
        try {
          const containerObj = {
            name: fakeString,
            containertype: 'SERVICE',
            status: 'PENDING',
            vendor: 'SIEMENS',
            parentSite: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/site',
                  key: {
                    type: 'oci/siteKey',
                    keyValue: 34
                  }
                }
              }
            ]
          };
          a.createContainer(containerObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createContainer - no container type', (done) => {
        try {
          const containerObj = {
            name: fakeString,
            type: 'oci/container',
            status: 'PENDING',
            vendor: 'SIEMENS',
            parentSite: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/site',
                  key: {
                    type: 'oci/siteKey',
                    keyValue: 34
                  }
                }
              }
            ]
          };
          a.createContainer(containerObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'containertype\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createContainer - no status', (done) => {
        try {
          const containerObj = {
            name: fakeString,
            type: 'oci/container',
            containertype: 'SERVICE',
            vendor: 'SIEMENS',
            parentSite: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/site',
                  key: {
                    type: 'oci/siteKey',
                    keyValue: 34
                  }
                }
              }
            ]
          };
          a.createContainer(containerObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'status\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createContainer - no vendor', (done) => {
        try {
          const containerObj = {
            name: fakeString,
            type: 'oci/container',
            containertype: 'SERVICE',
            status: 'PENDING',
            parentSite: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/site',
                  key: {
                    type: 'oci/siteKey',
                    keyValue: 34
                  }
                }
              }
            ]
          };
          a.createContainer(containerObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'vendor\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createContainer - no parent site', (done) => {
        try {
          const containerObj = {
            name: fakeString,
            type: 'oci/container',
            containertype: 'SERVICE',
            status: 'PENDING',
            vendor: 'SIEMENS'
          };
          a.createContainer(containerObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'parentSite\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createContainerTemplate - errors', () => {
      it('should have a createContainerTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createContainerTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should createContainerTemplate - no data', (done) => {
        try {
          a.createContainerTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'templateData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-createContainerTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createContainerTemplate - no name', (done) => {
        try {
          const templateObj = {
            type: 'oci/containertemplate',
            containerTemplateType: 'SERVICE',
            status: 'PENDING',
            vendor: 'SIEMENS'
          };
          a.createContainerTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createContainerTemplate - no type', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            containerTemplateType: 'SERVICE',
            status: 'PENDING',
            vendor: 'SIEMENS'
          };
          a.createContainerTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createContainerTemplate - no container type', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            type: 'oci/containertemplate',
            status: 'PENDING',
            vendor: 'SIEMENS'
          };
          a.createContainerTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'containerTemplateType\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createContainerTemplate - no status', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            type: 'oci/containertemplate',
            containerTemplateType: 'SERVICE',
            vendor: 'SIEMENS'
          };
          a.createContainerTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'status\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createContainerTemplate - no vendor', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            type: 'oci/containertemplate',
            containerTemplateType: 'SERVICE',
            status: 'PENDING'
          };
          a.createContainerTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'vendor\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateContainer - errors', () => {
      it('should have a updateContainer function', (done) => {
        try {
          assert.equal(true, typeof a.updateContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should updateContainer - no id', (done) => {
        try {
          a.updateContainer(null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateContainer - no data', (done) => {
        try {
          a.updateContainer('123', null, null, null, (data, error) => {
            try {
              const displayE = 'containerData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateContainer - no type in data', (done) => {
        try {
          a.updateContainer('123', fakeObject, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateContainerTemplate - errors', () => {
      it('should have a updateContainerTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.updateContainerTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should updateContainerTemplate - no id', (done) => {
        try {
          a.updateContainerTemplate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateContainerTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateContainerTemplate - no data', (done) => {
        try {
          a.updateContainerTemplate('123', null, null, null, (data, error) => {
            try {
              const displayE = 'templateData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateContainerTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateContainerTemplate - no type in data', (done) => {
        try {
          a.updateContainerTemplate('123', fakeObject, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteContainer - errors', () => {
      it('should have a deleteContainer function', (done) => {
        try {
          assert.equal(true, typeof a.deleteContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should deleteContainer - no id', (done) => {
        try {
          a.deleteContainer(null, null, null, (data, error) => {
            try {
              const displayE = 'containerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-deleteContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteContainerTemplate - errors', () => {
      it('should have a deleteContainerTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteContainerTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should deleteContainerTemplate - no id', (done) => {
        try {
          a.deleteContainerTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-deleteContainerTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEquipments - errors', () => {
      it('should have a getEquipments function', (done) => {
        try {
          assert.equal(true, typeof a.getEquipments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getPorts - errors', () => {
      it('should have a getPorts function', (done) => {
        try {
          assert.equal(true, typeof a.getPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getPortTemplates - errors', () => {
      it('should have a getPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#createPort - errors', () => {
      it('should have a createPort function', (done) => {
        try {
          assert.equal(true, typeof a.createPort === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should createPort - no data', (done) => {
        try {
          a.createPort(null, null, null, (data, error) => {
            try {
              const displayE = 'portData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-createPort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createPort - no name in data', (done) => {
        try {
          const portObj = {
            type: 'oci/port',
            bandwidth: '1000BASET',
            connectortype: 'BNC',
            status: 'OK',
            parentCard: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/card',
                  key: {
                    type: 'oci/cardKey',
                    keyValue: 202
                  }
                }
              }
            ]
          };
          a.createPort(portObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createPort - no type in data', (done) => {
        try {
          const portObj = {
            name: fakeString,
            bandwidth: '1000BASET',
            connectortype: 'BNC',
            status: 'OK',
            parentCard: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/card',
                  key: {
                    type: 'oci/cardKey',
                    keyValue: 202
                  }
                }
              }
            ]
          };
          a.createPort(portObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createPort - no bandwidth in data', (done) => {
        try {
          const portObj = {
            name: fakeString,
            type: 'oci/port',
            connectortype: 'BNC',
            status: 'OK',
            parentCard: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/card',
                  key: {
                    type: 'oci/cardKey',
                    keyValue: 202
                  }
                }
              }
            ]
          };
          a.createPort(portObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'bandwidth\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createPort - no connector type in data', (done) => {
        try {
          const portObj = {
            name: fakeString,
            type: 'oci/port',
            bandwidth: '1000BASET',
            status: 'OK',
            parentCard: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/card',
                  key: {
                    type: 'oci/cardKey',
                    keyValue: 202
                  }
                }
              }
            ]
          };
          a.createPort(portObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'connectortype\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createPort - no status in data', (done) => {
        try {
          const portObj = {
            name: fakeString,
            type: 'oci/port',
            bandwidth: '1000BASET',
            connectortype: 'BNC',
            parentCard: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/card',
                  key: {
                    type: 'oci/cardKey',
                    keyValue: 202
                  }
                }
              }
            ]
          };
          a.createPort(portObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'status\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createPort - no parent card in data', (done) => {
        try {
          const portObj = {
            name: fakeString,
            type: 'oci/port',
            bandwidth: '1000BASET',
            connectortype: 'BNC',
            status: 'OK'
          };
          a.createPort(portObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'parentCard\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPortTemplate - errors', () => {
      it('should have a createPortTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createPortTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should createPortTemplate - no data', (done) => {
        try {
          a.createPortTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'templateData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-createPortTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createPortTemplate - no name in data', (done) => {
        try {
          const templateObj = {
            type: 'oci/port',
            bandwidth: '1000BASET',
            connectortype: 'BNC',
            status: 'OK'
          };
          a.createPortTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createPortTemplate - no type in data', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            bandwidth: '1000BASET',
            connectortype: 'BNC',
            status: 'OK'
          };
          a.createPortTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createPortTemplate - no bandwidth in data', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            type: 'oci/port',
            connectortype: 'BNC',
            status: 'OK'
          };
          a.createPortTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'bandwidth\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createPortTemplate - no connector type in data', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            type: 'oci/port',
            bandwidth: '1000BASET',
            status: 'OK'
          };
          a.createPortTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'connectortype\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createPortTemplate - no status in data', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            type: 'oci/port',
            bandwidth: '1000BASET',
            connectortype: 'BNC'
          };
          a.createPortTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'status\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePort - errors', () => {
      it('should have a updatePort function', (done) => {
        try {
          assert.equal(true, typeof a.updatePort === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should updatePort - no id', (done) => {
        try {
          a.updatePort(null, null, null, null, (data, error) => {
            try {
              const displayE = 'portId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updatePort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updatePort - no data', (done) => {
        try {
          a.updatePort('123', null, null, null, (data, error) => {
            try {
              const displayE = 'portData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updatePort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updatePort - no type in data', (done) => {
        try {
          a.updatePort('123', fakeObject, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePortTemplate - errors', () => {
      it('should have a updatePortTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.updatePortTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should updatePortTemplate - no id', (done) => {
        try {
          a.updatePortTemplate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updatePortTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updatePortTemplate - no data', (done) => {
        try {
          a.updatePortTemplate('123', null, null, null, (data, error) => {
            try {
              const displayE = 'templateData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updatePortTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updatePortTemplate - no type in data', (done) => {
        try {
          a.updatePortTemplate('123', fakeObject, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePort - errors', () => {
      it('should have a deletePort function', (done) => {
        try {
          assert.equal(true, typeof a.deletePort === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should deletePort - no id', (done) => {
        try {
          a.deletePort(null, null, null, (data, error) => {
            try {
              const displayE = 'portId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-deletePort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePortTemplate - errors', () => {
      it('should have a deletePortTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deletePortTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should deletePortTemplate - no id', (done) => {
        try {
          a.deletePortTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-deletePortTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getShelves - errors', () => {
      it('should have a getShelves function', (done) => {
        try {
          assert.equal(true, typeof a.getShelves === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getShelfTemplates - errors', () => {
      it('should have a getShelfTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getShelfTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#createShelf - errors', () => {
      it('should have a createShelf function', (done) => {
        try {
          assert.equal(true, typeof a.createShelf === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should createShelf - no data', (done) => {
        try {
          a.createShelf(null, null, null, (data, error) => {
            try {
              const displayE = 'shelfData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-createShelf', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createShelf - no name in data', (done) => {
        try {
          const shelfObj = {
            type: 'oci/shelf',
            shelftype: 'TYPE1',
            status: 'Ordered',
            vendor: 'VENDOR1',
            parentSite: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/site',
                  key: {
                    type: 'oci/siteKey',
                    keyValue: 34
                  }
                }
              }
            ]
          };
          a.createShelf(shelfObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createShelf - no type in data', (done) => {
        try {
          const shelfObj = {
            name: fakeString,
            shelftype: 'TYPE1',
            status: 'Ordered',
            vendor: 'VENDOR1',
            parentSite: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/site',
                  key: {
                    type: 'oci/siteKey',
                    keyValue: 34
                  }
                }
              }
            ]
          };
          a.createShelf(shelfObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createShelf - no shelf type in data', (done) => {
        try {
          const shelfObj = {
            name: fakeString,
            type: 'oci/shelf',
            status: 'Ordered',
            vendor: 'VENDOR1',
            parentSite: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/site',
                  key: {
                    type: 'oci/siteKey',
                    keyValue: 34
                  }
                }
              }
            ]
          };
          a.createShelf(shelfObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'shelftype\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createShelf - no status in data', (done) => {
        try {
          const shelfObj = {
            name: fakeString,
            type: 'oci/shelf',
            shelftype: 'TYPE1',
            vendor: 'VENDOR1',
            parentSite: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/site',
                  key: {
                    type: 'oci/siteKey',
                    keyValue: 34
                  }
                }
              }
            ]
          };
          a.createShelf(shelfObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'status\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createShelf - no vendor in data', (done) => {
        try {
          const shelfObj = {
            name: fakeString,
            type: 'oci/shelf',
            shelftype: 'TYPE1',
            status: 'Ordered',
            parentSite: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/site',
                  key: {
                    type: 'oci/siteKey',
                    keyValue: 34
                  }
                }
              }
            ]
          };
          a.createShelf(shelfObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'vendor\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createShelf - no parent site in data', (done) => {
        try {
          const shelfObj = {
            name: fakeString,
            type: 'oci/shelf',
            shelftype: 'TYPE1',
            status: 'Ordered',
            vendor: 'VENDOR1'
          };
          a.createShelf(shelfObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'parentSite\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createShelfTemplate - errors', () => {
      it('should have a createShelfTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createShelfTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should createShelfTemplate - no data', (done) => {
        try {
          a.createShelfTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'templateData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-createShelfTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createShelfTemplate - no name in data', (done) => {
        try {
          const templateObj = {
            type: 'oci/shelftemplate',
            shelfTemplateType: 'TYPE1',
            status: 'Ordered',
            vendor: 'VENDOR1'
          };
          a.createShelfTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createShelfTemplate - no type in data', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            shelfTemplateType: 'TYPE1',
            status: 'Ordered',
            vendor: 'VENDOR1'
          };
          a.createShelfTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createShelfTemplate - no shelf template type in data', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            type: 'oci/shelftemplate',
            status: 'Ordered',
            vendor: 'VENDOR1'
          };
          a.createShelfTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'shelfTemplateType\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createShelfTemplate - no status in data', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            type: 'oci/shelftemplate',
            shelfTemplateType: 'TYPE1',
            vendor: 'VENDOR1'
          };
          a.createShelfTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'status\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createShelfTemplate - no vendor in data', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            type: 'oci/shelftemplate',
            shelfTemplateType: 'TYPE1',
            status: 'Ordered'
          };
          a.createShelfTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'vendor\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateShelf - errors', () => {
      it('should have a updateShelf function', (done) => {
        try {
          assert.equal(true, typeof a.updateShelf === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should updateShelf - no id', (done) => {
        try {
          a.updateShelf(null, null, null, null, (data, error) => {
            try {
              const displayE = 'shelfId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateShelf', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateShelf - no data', (done) => {
        try {
          a.updateShelf('123', null, null, null, (data, error) => {
            try {
              const displayE = 'shelfData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateShelf', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateShelf - no type in data', (done) => {
        try {
          a.updateShelf('123', fakeObject, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateShelfTemplate - errors', () => {
      it('should have a updateShelfTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.updateShelfTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should updateShelfTemplate - no id', (done) => {
        try {
          a.updateShelfTemplate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateShelfTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateShelfTemplate - no data', (done) => {
        try {
          a.updateShelfTemplate('123', null, null, null, (data, error) => {
            try {
              const displayE = 'templateData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateShelfTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateShelfTemplate - no type in data', (done) => {
        try {
          a.updateShelfTemplate('123', fakeObject, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteShelf - errors', () => {
      it('should have a deleteShelf function', (done) => {
        try {
          assert.equal(true, typeof a.deleteShelf === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should deleteShelf - no id', (done) => {
        try {
          a.deleteShelf(null, null, null, (data, error) => {
            try {
              const displayE = 'shelfId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-deleteShelf', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteShelfTemplate - errors', () => {
      it('should have a deleteShelfTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteShelfTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should deleteShelfTemplate - no id', (done) => {
        try {
          a.deleteShelfTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-deleteShelfTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSites - errors', () => {
      it('should have a getSites function', (done) => {
        try {
          assert.equal(true, typeof a.getSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getSiteTemplates - errors', () => {
      it('should have a getSiteTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getSiteTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#createSite - errors', () => {
      it('should have a createSite function', (done) => {
        try {
          assert.equal(true, typeof a.createSite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should createSite - no data', (done) => {
        try {
          a.createSite(null, null, null, (data, error) => {
            try {
              const displayE = 'siteData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-createSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createSite - no name in data', (done) => {
        try {
          const siteObj = {
            type: 'oci/site',
            sitetype: 'OTHER',
            status: 'Ordered'
          };
          a.createSite(siteObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createSite - no type in data', (done) => {
        try {
          const siteObj = {
            name: fakeString,
            sitetype: 'OTHER',
            status: 'Ordered'
          };
          a.createSite(siteObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createSite - no sitetype in data', (done) => {
        try {
          const siteObj = {
            name: fakeString,
            type: 'oci/site',
            status: 'Ordered'
          };
          a.createSite(siteObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'sitetype\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createSite - no status in data', (done) => {
        try {
          const siteObj = {
            name: fakeString,
            type: 'oci/site',
            sitetype: 'OTHER'
          };
          a.createSite(siteObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'status\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSiteTemplate - errors', () => {
      it('should have a createSiteTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createSiteTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should createSiteTemplate - no data', (done) => {
        try {
          a.createSiteTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'templateData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-createSiteTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createSiteTemplate - no name in data', (done) => {
        try {
          const templateObj = {
            type: 'oci/sitetemplate',
            siteTemplateType: 'OTHER',
            status: 'Ordered'
          };
          a.createSiteTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createSiteTemplate - no type in data', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            siteTemplateType: 'OTHER',
            status: 'Ordered'
          };
          a.createSiteTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createSiteTemplate - no site template type in data', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            type: 'oci/sitetemplate',
            status: 'Ordered'
          };
          a.createSiteTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'siteTemplateType\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createSiteTemplate - no status in data', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            type: 'oci/sitetemplate',
            siteTemplateType: 'OTHER'
          };
          a.createSiteTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'status\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSite - errors', () => {
      it('should have a updateSite function', (done) => {
        try {
          assert.equal(true, typeof a.updateSite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should updateSite - no id', (done) => {
        try {
          a.updateSite(null, null, null, null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateSite - no data', (done) => {
        try {
          a.updateSite('123', null, null, null, (data, error) => {
            try {
              const displayE = 'siteData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateSite - no type in data', (done) => {
        try {
          a.updateSite('123', fakeObject, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSiteTemplate - errors', () => {
      it('should have a updateSiteTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.updateSiteTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should updateSiteTemplate - no id', (done) => {
        try {
          a.updateSiteTemplate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateSiteTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateSiteTemplate - no data', (done) => {
        try {
          a.updateSiteTemplate('123', null, null, null, (data, error) => {
            try {
              const displayE = 'templateData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateSiteTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateSiteTemplate - no type in data', (done) => {
        try {
          a.updateSiteTemplate('123', fakeObject, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSite - errors', () => {
      it('should have a deleteSite function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should deleteSite - no id', (done) => {
        try {
          a.deleteSite(null, null, null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-deleteSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSiteTemplate - errors', () => {
      it('should have a deleteSiteTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSiteTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should deleteSiteTemplate - no id', (done) => {
        try {
          a.deleteSiteTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-deleteSiteTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSlots - errors', () => {
      it('should have a getSlots function', (done) => {
        try {
          assert.equal(true, typeof a.getSlots === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getSlotTemplates - errors', () => {
      it('should have a getSlotTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getSlotTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#createSlot - errors', () => {
      it('should have a createSlot function', (done) => {
        try {
          assert.equal(true, typeof a.createSlot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should createSlot - no data', (done) => {
        try {
          a.createSlot(null, null, null, (data, error) => {
            try {
              const displayE = 'slotData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-createSlot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createSlot - no name in data', (done) => {
        try {
          const slotObj = {
            type: 'oci/slot',
            relOrder: 1,
            parentShelf: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/shelf',
                  key: {
                    type: 'oci/shelfKey',
                    keyValue: 70
                  }
                }
              }
            ]
          };
          a.createSlot(slotObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createSlot - no type in data', (done) => {
        try {
          const slotObj = {
            name: fakeString,
            relOrder: 1,
            parentShelf: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/shelf',
                  key: {
                    type: 'oci/shelfKey',
                    keyValue: 70
                  }
                }
              }
            ]
          };
          a.createSlot(slotObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createSlot - no relative order in data', (done) => {
        try {
          const slotObj = {
            name: fakeString,
            type: 'oci/slot',
            parentShelf: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/shelf',
                  key: {
                    type: 'oci/shelfKey',
                    keyValue: 70
                  }
                }
              }
            ]
          };
          a.createSlot(slotObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'relOrder\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createSlot - no parent site in data', (done) => {
        try {
          const slotObj = {
            name: fakeString,
            type: 'oci/slot',
            relOrder: 1
          };
          a.createSlot(slotObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'parentShelf\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSlotTemplate - errors', () => {
      it('should have a createSlotTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createSlotTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should createSlotTemplate - no data', (done) => {
        try {
          a.createSlotTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'templateData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-createSlotTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createSlotTemplate - no name in data', (done) => {
        try {
          const templateObj = {
            type: 'oci/slot',
            relOrder: 1
          };
          a.createSlotTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createSlotTemplate - no type in data', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            relOrder: 1
          };
          a.createSlotTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createSlotTemplate - no relative order in data', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            type: 'oci/slot'
          };
          a.createSlotTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'relOrder\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSlot - errors', () => {
      it('should have a updateSlot function', (done) => {
        try {
          assert.equal(true, typeof a.updateSlot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should updateSlot - no id', (done) => {
        try {
          a.updateSlot(null, null, null, null, (data, error) => {
            try {
              const displayE = 'slotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateSlot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateSlot - no data', (done) => {
        try {
          a.updateSlot('123', null, null, null, (data, error) => {
            try {
              const displayE = 'slotData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateSlot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateSlot - no type in data', (done) => {
        try {
          a.updateSlot('123', fakeObject, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSlotTemplate - errors', () => {
      it('should have a updateSlotTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.updateSlotTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should updateSlotTemplate - no id', (done) => {
        try {
          a.updateSlotTemplate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateSlotTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateSlotTemplate - no data', (done) => {
        try {
          a.updateSlotTemplate('123', null, null, null, (data, error) => {
            try {
              const displayE = 'templateData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateSlotTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateSlotTemplate - no type in data', (done) => {
        try {
          a.updateSlotTemplate('123', fakeObject, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSlot - errors', () => {
      it('should have a deleteSlot function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSlot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should deleteSlot - no id', (done) => {
        try {
          a.deleteSlot(null, null, null, (data, error) => {
            try {
              const displayE = 'slotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-deleteSlot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSlotTemplate - errors', () => {
      it('should have a deleteSlotTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSlotTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should deleteSlotTemplate - no id', (done) => {
        try {
          a.deleteSlotTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-deleteSlotTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrails - errors', () => {
      it('should have a getTrails function', (done) => {
        try {
          assert.equal(true, typeof a.getTrails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getTrailQuery - errors', () => {
      it('should have a getTrailQuery function', (done) => {
        try {
          assert.equal(true, typeof a.getTrailQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getTrailTemplates - errors', () => {
      it('should have a getTrailTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getTrailTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#createTrail - errors', () => {
      it('should have a createTrail function', (done) => {
        try {
          assert.equal(true, typeof a.createTrail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should createTrail - no data', (done) => {
        try {
          a.createTrail(null, null, null, (data, error) => {
            try {
              const displayE = 'trailData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-createTrail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createTrail - no name in data', (done) => {
        try {
          const trailObj = {
            type: 'occ/trail',
            trailtype: 'SERVICE',
            status: 'PENDING',
            bandwidth: '25 MBPS',
            assignmentType: 'NONE',
            protectionType: 'NONE'
          };
          a.createTrail(trailObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createTrail - no type in data', (done) => {
        try {
          const trailObj = {
            name: 'whatever',
            trailtype: 'SERVICE',
            status: 'PENDING',
            bandwidth: '25 MBPS',
            assignmentType: 'NONE',
            protectionType: 'NONE'
          };
          a.createTrail(trailObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createTrail - no trailtype in data', (done) => {
        try {
          const trailObj = {
            name: 'whatever',
            type: 'occ/trail',
            status: 'PENDING',
            bandwidth: '25 MBPS',
            assignmentType: 'NONE',
            protectionType: 'NONE'
          };
          a.createTrail(trailObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'trailtype\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createTrail - no status in data', (done) => {
        try {
          const trailObj = {
            name: 'whatever',
            type: 'occ/trail',
            trailtype: 'SERVICE',
            bandwidth: '25 MBPS',
            assignmentType: 'NONE',
            protectionType: 'NONE'
          };
          a.createTrail(trailObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'status\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createTrail - no bandwidth in data', (done) => {
        try {
          const trailObj = {
            name: 'whatever',
            type: 'occ/trail',
            trailtype: 'SERVICE',
            status: 'PENDING',
            assignmentType: 'NONE',
            protectionType: 'NONE'
          };
          a.createTrail(trailObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'bandwidth\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createTrail - no assignmentType in data', (done) => {
        try {
          const trailObj = {
            name: 'whatever',
            type: 'occ/trail',
            trailtype: 'SERVICE',
            status: 'PENDING',
            bandwidth: '25 MBPS',
            protectionType: 'NONE'
          };
          a.createTrail(trailObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'assignmentType\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createTrail - no protectionType in data', (done) => {
        try {
          const trailObj = {
            name: 'whatever',
            type: 'occ/trail',
            trailtype: 'SERVICE',
            status: 'PENDING',
            bandwidth: '25 MBPS',
            assignmentType: 'NONE'
          };
          a.createTrail(trailObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'protectionType\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTrailTemplate - errors', () => {
      it('should have a createTrailTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createTrailTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should createTrailTemplate - no data', (done) => {
        try {
          a.createTrailTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'templateData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-createTrailTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createTrailTemplate - no name in data', (done) => {
        try {
          const templateObj = {
            type: 'occ/trailtemplate',
            trailTemplateType: 'SERVICE',
            status: 'PENDING',
            bandwidth: '25 MBPS',
            assignmentType: 'NONE',
            protectionType: 'NONE'
          };
          a.createTrailTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'name\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createTrailTemplate - no type in data', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            trailTemplateType: 'SERVICE',
            status: 'PENDING',
            bandwidth: '25 MBPS',
            assignmentType: 'NONE',
            protectionType: 'NONE'
          };
          a.createTrailTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createTrailTemplate - no trailtype in data', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            type: 'occ/trail',
            status: 'PENDING',
            bandwidth: '25 MBPS',
            assignmentType: 'NONE',
            protectionType: 'NONE'
          };
          a.createTrailTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'trailTemplateType\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createTrailTemplate - no status in data', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            type: 'occ/trail',
            trailTemplateType: 'SERVICE',
            bandwidth: '25 MBPS',
            assignmentType: 'NONE',
            protectionType: 'NONE'
          };
          a.createTrailTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'status\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createTrailTemplate - no bandwidth in data', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            type: 'occ/trail',
            trailTemplateType: 'SERVICE',
            status: 'PENDING',
            assignmentType: 'NONE',
            protectionType: 'NONE'
          };
          a.createTrailTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'bandwidth\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createTrailTemplate - no assignmentType in data', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            type: 'occ/trail',
            trailTemplateType: 'SERVICE',
            status: 'PENDING',
            bandwidth: '25 MBPS',
            protectionType: 'NONE'
          };
          a.createTrailTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'assignmentType\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createTrailTemplate - no protectionType in data', (done) => {
        try {
          const templateObj = {
            name: fakeString,
            type: 'occ/trail',
            trailTemplateType: 'SERVICE',
            status: 'PENDING',
            bandwidth: '25 MBPS',
            assignmentType: 'NONE'
          };
          a.createTrailTemplate(templateObj, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'protectionType\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTrail - errors', () => {
      it('should have a updateTrail function', (done) => {
        try {
          assert.equal(true, typeof a.updateTrail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should updateTrail - no id', (done) => {
        try {
          a.updateTrail(null, null, null, null, (data, error) => {
            try {
              const displayE = 'trailId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateTrail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateTrail - no data', (done) => {
        try {
          a.updateTrail('123', null, null, null, (data, error) => {
            try {
              const displayE = 'trailData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateTrail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateTrail - no type', (done) => {
        try {
          a.updateTrail('123', fakeObject, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTrailBySite - errors', () => {
      it('should have a updateTrailBySite function', (done) => {
        try {
          assert.equal(true, typeof a.updateTrailBySite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should updateTrailBySite - no site query', (done) => {
        try {
          a.updateTrailBySite(null, null, null, null, (data, error) => {
            try {
              const displayE = 'siteQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateTrailBySite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateTrailBySite - no data', (done) => {
        try {
          a.updateTrailBySite({ name: 'abc123' }, null, null, null, (data, error) => {
            try {
              const displayE = 'trailData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateTrailBySite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTrailTemplate - errors', () => {
      it('should have a updateTrailTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.updateTrailTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should updateTrailTemplate - no id', (done) => {
        try {
          a.updateTrailTemplate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateTrailTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateTrailTemplate - no data', (done) => {
        try {
          a.updateTrailTemplate('123', null, null, null, (data, error) => {
            try {
              const displayE = 'templateData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateTrailTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateTrailTemplate - no type', (done) => {
        try {
          a.updateTrailTemplate('123', fakeObject, null, null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on must have required property \'type\'';
              runErrorAsserts(data, error, 'AD.312', 'Test-eai-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTrail - errors', () => {
      it('should have a deleteTrail function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTrail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should deleteTrail - no id', (done) => {
        try {
          a.deleteTrail(null, null, null, (data, error) => {
            try {
              const displayE = 'trailId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-deleteTrail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTrailTemplate - errors', () => {
      it('should have a deleteTrailTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTrailTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should deleteTrailTemplate - no id', (done) => {
        try {
          a.deleteTrailTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-deleteTrailTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGenerics - errors', () => {
      it('should have a getGenerics function', (done) => {
        try {
          assert.equal(true, typeof a.getGenerics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should getGenerics - no uriPath', (done) => {
        try {
          a.getGenerics(null, null, null, null, (data, error) => {
            try {
              const displayE = 'uriPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-getGenerics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGeneric - errors', () => {
      it('should have a createGeneric function', (done) => {
        try {
          assert.equal(true, typeof a.createGeneric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should createGeneric - no uriPath', (done) => {
        try {
          a.createGeneric(null, null, null, (data, error) => {
            try {
              const displayE = 'uriPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-createGeneric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should createGeneric - no data', (done) => {
        try {
          a.createGeneric('fakedata', null, null, (data, error) => {
            try {
              const displayE = 'genericData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-createGeneric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGeneric - errors', () => {
      it('should have a updateGeneric function', (done) => {
        try {
          assert.equal(true, typeof a.updateGeneric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should updateGeneric - no uriPath', (done) => {
        try {
          a.updateGeneric(null, null, null, null, (data, error) => {
            try {
              const displayE = 'uriPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateGeneric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateGeneric - no entityId', (done) => {
        try {
          a.updateGeneric('fakedata', null, null, null, (data, error) => {
            try {
              const displayE = 'entityId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateGeneric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should updateGeneric - no data', (done) => {
        try {
          a.updateGeneric('fakedata', 'fakedata', null, null, (data, error) => {
            try {
              const displayE = 'genericData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-updateGeneric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGeneric - errors', () => {
      it('should have a deleteGeneric function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGeneric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should deleteGeneric - no uriPath', (done) => {
        try {
          a.deleteGeneric(null, null, null, (data, error) => {
            try {
              const displayE = 'uriPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-deleteGeneric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should deleteGeneric - no id', (done) => {
        try {
          a.deleteGeneric('fakedata', null, null, (data, error) => {
            try {
              const displayE = 'entityId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-deleteGeneric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#processRequest - errors', () => {
      const qparam = {
        blah: 'blah'
      };
      const payload = {
        blah: 'blah'
      };
      it('should have a processRequest function', (done) => {
        try {
          assert.equal(true, typeof a.processRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should processRequest - no entity', (done) => {
        try {
          a.processRequest(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'entity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - no action', (done) => {
        try {
          a.processRequest('blah', null, null, null, null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - card fetch with no query', (done) => {
        try {
          a.processRequest('card', 'fetchByKey', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - card fetch with no query key', (done) => {
        try {
          a.processRequest('card', 'fetchByKey', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - card create with no body', (done) => {
        try {
          a.processRequest('card', 'create', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - card update with no body', (done) => {
        try {
          a.processRequest('card', 'update', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - card update with no query', (done) => {
        try {
          a.processRequest('card', 'update', null, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - card update with no query key', (done) => {
        try {
          a.processRequest('card', 'update', qparam, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - card delete with no query', (done) => {
        try {
          a.processRequest('card', 'delete', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - card delete with no query key', (done) => {
        try {
          a.processRequest('card', 'delete', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - card invalid action', (done) => {
        try {
          a.processRequest('card', 'badaction', null, null, null, (data, error) => {
            try {
              const displayE = 'Provided Action: badaction not supported';
              runErrorAsserts(data, error, 'AD.309', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - cardTemplate fetch with no query', (done) => {
        try {
          a.processRequest('cardTemplate', 'fetchByKey', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - cardTemplate fetch with no query key', (done) => {
        try {
          a.processRequest('cardTemplate', 'fetchByKey', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - cardTemplate create with no body', (done) => {
        try {
          a.processRequest('cardTemplate', 'create', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - cardTemplate update with no body', (done) => {
        try {
          a.processRequest('cardTemplate', 'update', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - cardTemplate update with no query', (done) => {
        try {
          a.processRequest('cardTemplate', 'update', null, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - cardTemplate update with no query key', (done) => {
        try {
          a.processRequest('cardTemplate', 'update', qparam, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - cardTemplate delete with no query', (done) => {
        try {
          a.processRequest('cardTemplate', 'delete', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - cardTemplate delete with no query key', (done) => {
        try {
          a.processRequest('cardTemplate', 'delete', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - cardTemplate invalid action', (done) => {
        try {
          a.processRequest('cardTemplate', 'badaction', null, null, null, (data, error) => {
            try {
              const displayE = 'Provided Action: badaction not supported';
              runErrorAsserts(data, error, 'AD.309', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - container fetch with no query', (done) => {
        try {
          a.processRequest('container', 'fetchByKey', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - container fetch with no query key', (done) => {
        try {
          a.processRequest('container', 'fetchByKey', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - container create with no body', (done) => {
        try {
          a.processRequest('container', 'create', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - container update with no body', (done) => {
        try {
          a.processRequest('container', 'update', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - container update with no query', (done) => {
        try {
          a.processRequest('container', 'update', null, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - container update with no query key', (done) => {
        try {
          a.processRequest('container', 'update', qparam, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - container delete with no query', (done) => {
        try {
          a.processRequest('container', 'delete', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - container delete with no query key', (done) => {
        try {
          a.processRequest('container', 'delete', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - container invalid action', (done) => {
        try {
          a.processRequest('container', 'badaction', null, null, null, (data, error) => {
            try {
              const displayE = 'Provided Action: badaction not supported';
              runErrorAsserts(data, error, 'AD.309', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - containerTemplate fetch with no query', (done) => {
        try {
          a.processRequest('containerTemplate', 'fetchByKey', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - containerTemplate fetch with no query key', (done) => {
        try {
          a.processRequest('containerTemplate', 'fetchByKey', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - containerTemplate create with no body', (done) => {
        try {
          a.processRequest('containerTemplate', 'create', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - containerTemplate update with no body', (done) => {
        try {
          a.processRequest('containerTemplate', 'update', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - containerTemplate update with no query', (done) => {
        try {
          a.processRequest('containerTemplate', 'update', null, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - containerTemplate update with no query key', (done) => {
        try {
          a.processRequest('containerTemplate', 'update', qparam, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - containerTemplate delete with no query', (done) => {
        try {
          a.processRequest('containerTemplate', 'delete', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - containerTemplate delete with no query key', (done) => {
        try {
          a.processRequest('containerTemplate', 'delete', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - containerTemplate invalid action', (done) => {
        try {
          a.processRequest('containerTemplate', 'badaction', null, null, null, (data, error) => {
            try {
              const displayE = 'Provided Action: badaction not supported';
              runErrorAsserts(data, error, 'AD.309', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - equipment fetch with no query', (done) => {
        try {
          a.processRequest('equipment', 'fetchByKey', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - equipment fetch with no query key', (done) => {
        try {
          a.processRequest('equipment', 'fetchByKey', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - equipment create with no body', (done) => {
        try {
          a.processRequest('equipment', 'create', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - equipment update with no body', (done) => {
        try {
          a.processRequest('equipment', 'update', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - equipment update with no query', (done) => {
        try {
          a.processRequest('equipment', 'update', null, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - equipment update with no query key', (done) => {
        try {
          a.processRequest('equipment', 'update', qparam, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - equipment delete with no query', (done) => {
        try {
          a.processRequest('equipment', 'delete', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - equipment delete with no query key', (done) => {
        try {
          a.processRequest('equipment', 'delete', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - equipment invalid action', (done) => {
        try {
          a.processRequest('equipment', 'badaction', null, null, null, (data, error) => {
            try {
              const displayE = 'Provided Action: badaction not supported';
              runErrorAsserts(data, error, 'AD.309', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - port fetch with no query', (done) => {
        try {
          a.processRequest('port', 'fetchByKey', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - port fetch with no query key', (done) => {
        try {
          a.processRequest('port', 'fetchByKey', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - port create with no body', (done) => {
        try {
          a.processRequest('port', 'create', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - port update with no body', (done) => {
        try {
          a.processRequest('port', 'update', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - port update with no query', (done) => {
        try {
          a.processRequest('port', 'update', null, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - port update with no query key', (done) => {
        try {
          a.processRequest('port', 'update', qparam, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - port delete with no query', (done) => {
        try {
          a.processRequest('port', 'delete', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - port delete with no query key', (done) => {
        try {
          a.processRequest('port', 'delete', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - port invalid action', (done) => {
        try {
          a.processRequest('port', 'badaction', null, null, null, (data, error) => {
            try {
              const displayE = 'Provided Action: badaction not supported';
              runErrorAsserts(data, error, 'AD.309', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - shelf fetch with no query', (done) => {
        try {
          a.processRequest('shelf', 'fetchByKey', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - shelf fetch with no query key', (done) => {
        try {
          a.processRequest('shelf', 'fetchByKey', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - shelf create with no body', (done) => {
        try {
          a.processRequest('shelf', 'create', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - shelf update with no body', (done) => {
        try {
          a.processRequest('shelf', 'update', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - shelf update with no query', (done) => {
        try {
          a.processRequest('shelf', 'update', null, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - shelf update with no query key', (done) => {
        try {
          a.processRequest('shelf', 'update', qparam, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - shelf delete with no query', (done) => {
        try {
          a.processRequest('shelf', 'delete', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - shelf delete with no query key', (done) => {
        try {
          a.processRequest('shelf', 'delete', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - shelf invalid action', (done) => {
        try {
          a.processRequest('shelf', 'badaction', null, null, null, (data, error) => {
            try {
              const displayE = 'Provided Action: badaction not supported';
              runErrorAsserts(data, error, 'AD.309', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - shelfTemplate fetch with no query', (done) => {
        try {
          a.processRequest('shelfTemplate', 'fetchByKey', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - shelfTemplate fetch with no query key', (done) => {
        try {
          a.processRequest('shelfTemplate', 'fetchByKey', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - shelfTemplate create with no body', (done) => {
        try {
          a.processRequest('shelfTemplate', 'create', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - shelfTemplate update with no body', (done) => {
        try {
          a.processRequest('shelfTemplate', 'update', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - shelfTemplate update with no query', (done) => {
        try {
          a.processRequest('shelfTemplate', 'update', null, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - shelfTemplate update with no query key', (done) => {
        try {
          a.processRequest('shelfTemplate', 'update', qparam, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - shelfTemplate delete with no query', (done) => {
        try {
          a.processRequest('shelfTemplate', 'delete', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - shelfTemplate delete with no query key', (done) => {
        try {
          a.processRequest('shelfTemplate', 'delete', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - shelfTemplate invalid action', (done) => {
        try {
          a.processRequest('shelfTemplate', 'badaction', null, null, null, (data, error) => {
            try {
              const displayE = 'Provided Action: badaction not supported';
              runErrorAsserts(data, error, 'AD.309', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - site fetch with no query', (done) => {
        try {
          a.processRequest('site', 'fetchByKey', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - site fetch with no query key', (done) => {
        try {
          a.processRequest('site', 'fetchByKey', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - site create with no body', (done) => {
        try {
          a.processRequest('site', 'create', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - site update with no body', (done) => {
        try {
          a.processRequest('site', 'update', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - site update with no query', (done) => {
        try {
          a.processRequest('site', 'update', null, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - site update with no query key', (done) => {
        try {
          a.processRequest('site', 'update', qparam, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - site delete with no query', (done) => {
        try {
          a.processRequest('site', 'delete', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - site delete with no query key', (done) => {
        try {
          a.processRequest('site', 'delete', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - site invalid action', (done) => {
        try {
          a.processRequest('site', 'badaction', null, null, null, (data, error) => {
            try {
              const displayE = 'Provided Action: badaction not supported';
              runErrorAsserts(data, error, 'AD.309', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - slot fetch with no query', (done) => {
        try {
          a.processRequest('slot', 'fetchByKey', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - slot fetch with no query key', (done) => {
        try {
          a.processRequest('slot', 'fetchByKey', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - slot create with no body', (done) => {
        try {
          a.processRequest('slot', 'create', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - slot update with no body', (done) => {
        try {
          a.processRequest('slot', 'update', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - slot update with no query', (done) => {
        try {
          a.processRequest('slot', 'update', null, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - slot update with no query key', (done) => {
        try {
          a.processRequest('slot', 'update', qparam, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - slot delete with no query', (done) => {
        try {
          a.processRequest('slot', 'delete', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - slot delete with no query key', (done) => {
        try {
          a.processRequest('slot', 'delete', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - slot invalid action', (done) => {
        try {
          a.processRequest('slot', 'badaction', null, null, null, (data, error) => {
            try {
              const displayE = 'Provided Action: badaction not supported';
              runErrorAsserts(data, error, 'AD.309', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - slotTemplate fetch with no query', (done) => {
        try {
          a.processRequest('slotTemplate', 'fetchByKey', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - slotTemplate fetch with no query key', (done) => {
        try {
          a.processRequest('slotTemplate', 'fetchByKey', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - slotTemplate create with no body', (done) => {
        try {
          a.processRequest('slotTemplate', 'create', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - slotTemplate update with no body', (done) => {
        try {
          a.processRequest('slotTemplate', 'update', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - slotTemplate update with no query', (done) => {
        try {
          a.processRequest('slotTemplate', 'update', null, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - slotTemplate update with no query key', (done) => {
        try {
          a.processRequest('slotTemplate', 'update', qparam, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - slotTemplate delete with no query', (done) => {
        try {
          a.processRequest('slotTemplate', 'delete', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - slotTemplate delete with no query key', (done) => {
        try {
          a.processRequest('slotTemplate', 'delete', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - slotTemplate invalid action', (done) => {
        try {
          a.processRequest('slotTemplate', 'badaction', null, null, null, (data, error) => {
            try {
              const displayE = 'Provided Action: badaction not supported';
              runErrorAsserts(data, error, 'AD.309', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - trail fetch with no query', (done) => {
        try {
          a.processRequest('trail', 'fetchByKey', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - trail fetch with no query key', (done) => {
        try {
          a.processRequest('trail', 'fetchByKey', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - trail create with no body', (done) => {
        try {
          a.processRequest('trail', 'create', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - trail update with no body', (done) => {
        try {
          a.processRequest('trail', 'update', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - trail update with no query', (done) => {
        try {
          a.processRequest('trail', 'update', null, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - trail update with no query key', (done) => {
        try {
          a.processRequest('trail', 'update', qparam, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - trail delete with no query', (done) => {
        try {
          a.processRequest('trail', 'delete', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - trail delete with no query key', (done) => {
        try {
          a.processRequest('trail', 'delete', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - trail invalid action', (done) => {
        try {
          a.processRequest('trail', 'badaction', null, null, null, (data, error) => {
            try {
              const displayE = 'Provided Action: badaction not supported';
              runErrorAsserts(data, error, 'AD.309', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - trailTemplate fetch with no query', (done) => {
        try {
          a.processRequest('trailTemplate', 'fetchByKey', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - trailTemplate fetch with no query key', (done) => {
        try {
          a.processRequest('trailTemplate', 'fetchByKey', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - trailTemplate create with no body', (done) => {
        try {
          a.processRequest('trailTemplate', 'create', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - trailTemplate update with no body', (done) => {
        try {
          a.processRequest('trailTemplate', 'update', null, null, null, (data, error) => {
            try {
              const displayE = 'bodyParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - trailTemplate update with no query', (done) => {
        try {
          a.processRequest('trailTemplate', 'update', null, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - trailTemplate update with no query key', (done) => {
        try {
          a.processRequest('trailTemplate', 'update', qparam, payload, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - trailTemplate delete with no query', (done) => {
        try {
          a.processRequest('trailTemplate', 'delete', null, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - trailTemplate delete with no query key', (done) => {
        try {
          a.processRequest('trailTemplate', 'delete', qparam, null, null, (data, error) => {
            try {
              const displayE = 'queryParams with key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - trailTemplate invalid action', (done) => {
        try {
          a.processRequest('trailTemplate', 'badaction', null, null, null, (data, error) => {
            try {
              const displayE = 'Provided Action: badaction not supported';
              runErrorAsserts(data, error, 'AD.309', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should processRequest - invalid entity', (done) => {
        try {
          const myParam = {
            key: 'fake'
          };
          a.processRequest('badentity', 'delete', myParam, null, null, (data, error) => {
            try {
              const displayE = 'Provided Entity: badentity not supported';
              runErrorAsserts(data, error, 'AD.308', 'Test-eai-adapter-processRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
