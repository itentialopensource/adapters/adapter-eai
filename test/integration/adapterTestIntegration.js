/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-eai',
      type: 'EAI',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const EAI = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] EAI Adapter Test', () => {
  describe('EAI Class Tests', () => {
    const a = new EAI(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-eai-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-eai-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    let siteId = 1;
    let siteNm = `test${Math.random()}`;

    describe('#createSite', () => {
      it('should createSite', (done) => {
        try {
          const siteObj = {
            name: siteNm,
            type: 'oci/site',
            sitetype: 'OTHER',
            status: 'Ordered'
          };
          a.createSite(siteObj, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(siteId, data.response.id);
                siteNm = data.response.name;
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              siteId = data.response.id;
              saveMockData('site', 'createSite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSites', () => {
      it('should getSites', (done) => {
        try {
          a.getSites(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(siteId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.notEqual(undefined, data.response[0].id);
                assert.notEqual(null, data.response[0].id);
                assert.notEqual('', data.response[0].id);
              }
              saveMockData('site', 'getSites', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should getSites by id', (done) => {
        try {
          a.getSites(siteId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(siteId, data.response.id);
              } else {
                assert.equal(siteNm, data.response.name);
              }
              saveMockData('site', 'getSites', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSite', () => {
      it('should updateSite', (done) => {
        try {
          const usiteObj = {
            name: `${siteNm}.updated`,
            type: 'oci/site'
          };
          const queryP = {
            fs: 'dynAttrs,derivedAttrs'
          };
          a.updateSite(siteId, usiteObj, queryP, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(siteId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              siteNm = usiteObj.name;
              saveMockData('site', 'updateSite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSites', () => {
      it('should getSites by filter', (done) => {
        try {
          const filterObj = {
            name: siteNm
          };
          a.getSites(null, filterObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(siteId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.equal(siteId, data.response[0].id);
                assert.equal(siteNm, data.response[0].name);
              }
              saveMockData('site', 'getSites', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let siteTId = 1;
    let siteTNm = `test${Math.random()}`;

    describe('#createSiteTemplate', () => {
      it('should createSiteTemplate', (done) => {
        try {
          const templateObj = {
            name: siteTNm,
            type: 'oci/sitetemplate',
            siteTemplateType: 'OTHER',
            status: 'Ordered'
          };
          a.createSiteTemplate(templateObj, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(siteTId, data.response.id);
                siteTNm = data.response.name;
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              siteTId = data.response.id;
              saveMockData('site', 'createSiteTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteTemplates', () => {
      it('should getSiteTemplates', (done) => {
        try {
          a.getSiteTemplates(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(siteTId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.notEqual(undefined, data.response[0].id);
                assert.notEqual(null, data.response[0].id);
                assert.notEqual('', data.response[0].id);
              }
              saveMockData('site', 'getSiteTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should getSiteTemplates by id', (done) => {
        try {
          a.getSiteTemplates(siteTId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(siteTId, data.response.id);
              } else {
                assert.equal(siteTNm, data.response.name);
              }
              saveMockData('site', 'getSiteTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSiteTemplate', () => {
      it('should updateSiteTemplate', (done) => {
        try {
          const utemplateObj = {
            name: `${siteTNm}.updated`,
            type: 'oci/sitetemplate'
          };
          const queryP = {
            fs: 'dynAttrs,derivedAttrs'
          };
          a.updateSiteTemplate(siteTId, utemplateObj, queryP, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(siteTId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              siteTNm = utemplateObj.name;
              saveMockData('site', 'updateSiteTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteTemplates', () => {
      it('should getSiteTemplates by filter', (done) => {
        try {
          const filterObj = {
            name: siteTNm
          };
          a.getSiteTemplates(null, filterObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(siteTId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.equal(siteTId, data.response[0].id);
                assert.equal(siteTNm, data.response[0].name);
              }
              saveMockData('site', 'getSiteTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSiteTemplate', () => {
      it('should deleteSiteTemplate', (done) => {
        try {
          a.deleteSiteTemplate(siteTId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('site', 'deleteSiteTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let shelfId = 32;
    let shelfNm = `test${Math.random()}`;

    describe('#createShelf', () => {
      it('should createShelf', (done) => {
        try {
          const shelfObj = {
            name: shelfNm,
            type: 'oci/shelf',
            shelftype: 'ROUTER',
            status: 'Ordered',
            vendor: 'SIEMENS',
            parentSite: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/site',
                  key: {
                    type: 'oci/siteKey',
                    keyValue: siteId
                  }
                }
              }
            ]
          };
          a.createShelf(shelfObj, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(shelfId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              shelfId = data.response.id;
              saveMockData('shelf', 'createShelf', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getShelves', () => {
      it('should getShelves', (done) => {
        try {
          a.getShelves(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(shelfId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.notEqual(undefined, data.response[0].id);
                assert.notEqual(null, data.response[0].id);
                assert.notEqual('', data.response[0].id);
              }
              saveMockData('shelf', 'getShelves', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should getShelves by id', (done) => {
        try {
          a.getShelves(shelfId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(shelfId, data.response.id);
              } else {
                assert.equal(shelfNm, data.response.name);
              }
              saveMockData('shelf', 'getShelves', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateShelf', () => {
      it('should updateShelf', (done) => {
        try {
          const ushelfObj = {
            name: `${shelfNm}.updated`,
            type: 'oci/shelf'
          };
          const queryP = {
            fs: 'dynAttrs,derivedAttrs'
          };
          a.updateShelf(shelfId, ushelfObj, queryP, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(shelfId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              shelfNm = ushelfObj.name;
              saveMockData('shelf', 'updateShelf', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getShelves', () => {
      it('should getShelves by filter', (done) => {
        try {
          const filterObj = {
            name: shelfNm
          };
          a.getShelves(null, filterObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(shelfId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.equal(shelfId, data.response[0].id);
                assert.equal(shelfNm, data.response[0].name);
              }
              saveMockData('shelf', 'getShelves', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let shelfTId = 1;
    let shelfTNm = `test${Math.random()}`;

    describe('#createShelfTemplate', () => {
      it('should createShelfTemplate', (done) => {
        try {
          const templateObj = {
            name: shelfTNm,
            type: 'oci/shelftemplate',
            shelfTemplateType: 'ROUTER',
            status: 'Ordered',
            vendor: 'SIEMENS'
          };
          a.createShelfTemplate(templateObj, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(shelfTId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              shelfTId = data.response.id;
              saveMockData('shelf', 'createShelfTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getShelfTemplates', () => {
      it('should getShelfTemplates', (done) => {
        try {
          a.getShelfTemplates(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(shelfTId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.notEqual(undefined, data.response[0].id);
                assert.notEqual(null, data.response[0].id);
                assert.notEqual('', data.response[0].id);
              }
              saveMockData('shelf', 'getShelfTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should getShelfTemplates by id', (done) => {
        try {
          a.getShelfTemplates(shelfTId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(shelfTId, data.response.id);
              } else {
                assert.equal(shelfTNm, data.response.name);
              }
              saveMockData('shelf', 'getShelfTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateShelfTemplate', () => {
      it('should updateShelfTemplate', (done) => {
        try {
          const utemplateObj = {
            name: `${shelfTNm}.updated`,
            type: 'oci/shelftemplate'
          };
          const queryP = {
            fs: 'dynAttrs,derivedAttrs'
          };
          a.updateShelfTemplate(shelfTId, utemplateObj, queryP, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(shelfTId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              shelfTNm = utemplateObj.name;
              saveMockData('shelf', 'updateShelfTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getShelfTemplates', () => {
      it('should getShelfTemplates by filter', (done) => {
        try {
          const filterObj = {
            name: shelfTNm
          };
          a.getShelfTemplates(null, filterObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(shelfTId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.equal(shelfTId, data.response[0].id);
                assert.equal(shelfTNm, data.response[0].name);
              }
              saveMockData('shelf', 'getShelfTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteShelfTemplate', () => {
      it('should deleteShelfTemplate', (done) => {
        try {
          a.deleteShelfTemplate(shelfTId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('shelf', 'deleteShelfTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let containerId = 1;
    let containerNm = `test${Math.random()}`;

    describe('#createContainer', () => {
      it('should createContainer', (done) => {
        try {
          const containerObj = {
            name: containerNm,
            type: 'oci/container',
            containertype: 'ROUTER',
            status: 'Ordered',
            vendor: 'SIEMENS',
            parentSite: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/site',
                  key: {
                    type: 'oci/siteKey',
                    keyValue: siteId
                  }
                }
              }
            ]
          };
          a.createContainer(containerObj, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(containerId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              containerId = data.response.id;
              saveMockData('container', 'createContainer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContainers', () => {
      it('should getContainers', (done) => {
        try {
          a.getContainers(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(containerId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.notEqual(undefined, data.response[0].id);
                assert.notEqual(null, data.response[0].id);
                assert.notEqual('', data.response[0].id);
              }
              saveMockData('container', 'getContainers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should getContainers by id', (done) => {
        try {
          a.getContainers(containerId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(containerId, data.response.id);
              } else {
                assert.equal(containerNm, data.response.name);
              }
              saveMockData('container', 'getContainers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateContainer', () => {
      it('should updateContainer', (done) => {
        try {
          const ucontainerObj = {
            name: `${containerNm}.updated`,
            type: 'oci/container'
          };
          const queryP = {
            fs: 'dynAttrs,derivedAttrs'
          };
          a.updateContainer(containerId, ucontainerObj, queryP, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(containerId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              containerNm = ucontainerObj.name;
              saveMockData('container', 'updateContainer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContainers', () => {
      it('should getContainers by filter', (done) => {
        try {
          const filterObj = {
            name: containerNm
          };
          a.getContainers(null, filterObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(containerId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.equal(containerNm, data.response[0].name);
              }
              saveMockData('container', 'getContainers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let containerTId = 3;
    let containerTNm = `test${Math.random()}`;

    describe('#createContainerTemplate', () => {
      it('should createContainerTemplate', (done) => {
        try {
          const templateObj = {
            name: containerTNm,
            type: 'oci/containertemplate',
            containerTemplateType: 'ROUTER',
            status: 'Ordered',
            vendor: 'SIEMENS'
          };
          a.createContainerTemplate(templateObj, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(containerTId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              containerTId = data.response.id;
              saveMockData('container', 'createContainerTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContainerTemplates', () => {
      it('should getContainerTemplates', (done) => {
        try {
          a.getContainerTemplates(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(containerTId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.notEqual(undefined, data.response[0].id);
                assert.notEqual(null, data.response[0].id);
                assert.notEqual('', data.response[0].id);
              }
              saveMockData('container', 'getContainerTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should getContainerTemplates by id', (done) => {
        try {
          a.getContainerTemplates(containerTId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(containerTId, data.response.id);
              } else {
                assert.equal(containerTNm, data.response.name);
              }
              saveMockData('container', 'getContainerTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateContainerTemplate', () => {
      it('should updateContainerTemplate', (done) => {
        try {
          const utemplateObj = {
            name: `${containerTNm}.updated`,
            type: 'oci/containertemplate'
          };
          const queryP = {
            fs: 'dynAttrs,derivedAttrs'
          };
          a.updateContainerTemplate(containerTId, utemplateObj, queryP, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(containerTId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              containerTNm = utemplateObj.name;
              saveMockData('container', 'updateContainerTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContainerTemplates', () => {
      it('should getContainerTemplates by filter', (done) => {
        try {
          const filterObj = {
            name: containerTNm
          };
          a.getContainerTemplates(null, filterObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(containerTId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.equal(containerTNm, data.response[0].name);
              }
              saveMockData('container', 'getContainerTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteContainerTemplate', () => {
      it('should deleteContainerTemplate', (done) => {
        try {
          a.deleteContainerTemplate(containerTId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('container', 'deleteContainerTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let slotId = 1;
    let slotNm = `test${Math.random()}`;

    describe('#createSlot', () => {
      it('should createSlot', (done) => {
        try {
          const slotObj = {
            type: 'oci/slot',
            name: slotNm,
            relOrder: 1,
            parentShelf: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/shelf',
                  key: {
                    type: 'oci/shelfKey',
                    keyValue: shelfId
                  }
                }
              }
            ]
          };
          a.createSlot(slotObj, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(slotId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              slotId = data.response.id;
              saveMockData('slot', 'createSlot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSlots', () => {
      it('should getSlots', (done) => {
        try {
          a.getSlots(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(slotId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.notEqual(undefined, data.response[0].id);
                assert.notEqual(null, data.response[0].id);
                assert.notEqual('', data.response[0].id);
              }
              saveMockData('slot', 'getSlots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should getSlots by id', (done) => {
        try {
          a.getSlots(slotId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(slotId, data.response.id);
              } else {
                assert.equal(slotNm, data.response.name);
              }
              saveMockData('slot', 'getSlots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSlot', () => {
      it('should updateSlot', (done) => {
        try {
          const uslotObj = {
            name: `${slotNm}.updated`,
            type: 'oci/slot'
          };
          const queryP = {
            fs: 'dynAttrs,derivedAttrs'
          };
          a.updateSlot(slotId, uslotObj, queryP, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(slotId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              slotNm = uslotObj.name;
              saveMockData('slot', 'updateSlot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSlots', () => {
      it('should getSlots by filter', (done) => {
        try {
          const filterObj = {
            name: slotNm
          };
          a.getSlots(null, filterObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(slotId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.equal(slotId, data.response[0].id);
                assert.equal(slotNm, data.response[0].name);
              }
              saveMockData('slot', 'getSlots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let slotTId = 1;
    let slotTNm = `test${Math.random()}`;

    describe('#createSlotTemplate', () => {
      it('should createSlotTemplate', (done) => {
        try {
          const templateObj = {
            type: 'oci/slottemplate',
            name: slotTNm,
            relOrder: 1
          };
          a.createSlotTemplate(templateObj, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(slotTId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              slotTId = data.response.id;
              saveMockData('slot', 'createSlotTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSlotTemplates', () => {
      it('should getSlotTemplates', (done) => {
        try {
          a.getSlotTemplates(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(slotTId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.notEqual(undefined, data.response[0].id);
                assert.notEqual(null, data.response[0].id);
                assert.notEqual('', data.response[0].id);
              }
              saveMockData('slot', 'getSlotTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should getSlotTemplates by id', (done) => {
        try {
          a.getSlotTemplates(slotTId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(slotTId, data.response.id);
              } else {
                assert.equal(slotTNm, data.response.name);
              }
              saveMockData('slot', 'getSlotTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSlotTemplate', () => {
      it('should updateSlotTemplate', (done) => {
        try {
          const utemplateObj = {
            name: `${slotTNm}.updated`,
            type: 'oci/slottemplate'
          };
          const queryP = {
            fs: 'dynAttrs,derivedAttrs'
          };
          a.updateSlotTemplate(slotTId, utemplateObj, queryP, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(slotTId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              slotTNm = utemplateObj.name;
              saveMockData('slot', 'updateSlotTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSlotTemplates', () => {
      it('should getSlotTemplates by filter', (done) => {
        try {
          const filterObj = {
            name: slotTNm
          };
          a.getSlotTemplates(null, filterObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(slotTId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.equal(slotTId, data.response[0].id);
                assert.equal(slotTNm, data.response[0].name);
              }
              saveMockData('slot', 'getSlotTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSlotTemplate', () => {
      it('should deleteSlotTemplate', (done) => {
        try {
          a.deleteSlotTemplate(slotTId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('container', 'deleteSlotTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let cardId = 1;
    let cardNm = `test${Math.random()}`;

    describe('#createCard', () => {
      it('should createCard', (done) => {
        try {
          const cardObj = {
            name: cardNm,
            type: 'oci/card',
            slotOccupancy: 1,
            cardtype: 'CardType1',
            status: 'Ordered',
            parentSlots: [
              {
                mode: 'ADDED',
                value: {
                  type: 'oci/slot',
                  key: {
                    type: 'oci/slotKey',
                    keyValue: slotId
                  }
                }
              }
            ]
          };
          a.createCard(cardObj, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(cardId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual('', data.response.id);
              }
              cardId = data.response.id;
              saveMockData('card', 'createCard', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCards', () => {
      it('should getCards', (done) => {
        try {
          a.getCards(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(4, data.response.length);
                assert.equal(cardId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.notEqual(undefined, data.response[0].id);
                assert.notEqual(null, data.response[0].id);
                assert.notEqual('', data.response[0].id);
              }
              saveMockData('card', 'getCards', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should getCards by id', (done) => {
        try {
          a.getCards(cardId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(cardId, data.response.id);
              } else {
                assert.equal(cardNm, data.response.name);
              }
              saveMockData('card', 'getCards', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCard', () => {
      it('should updateCard', (done) => {
        try {
          const ucardObj = {
            name: `${cardNm}.updated`,
            type: 'oci/card'
          };
          const queryP = {
            fs: 'dynAttrs,derivedAttrs'
          };
          a.updateCard(cardId, ucardObj, queryP, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(cardId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              cardNm = ucardObj.name;
              saveMockData('card', 'updateCard', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCards', () => {
      it('should getCards by filter', (done) => {
        try {
          const filterObj = {
            name: cardNm
          };
          a.getCards(null, filterObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(cardId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.equal(cardId, data.response[0].id);
                assert.equal(cardNm, data.response[0].name);
              }
              saveMockData('card', 'getCards', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let cardTId = 1;
    let cardTNm = `test${Math.random()}`;

    describe('#createCardTemplate', () => {
      it('should createCardTemplate', (done) => {
        try {
          const cardTObj = {
            name: cardTNm,
            type: 'oci/cardtemplate',
            cardTemplateType: 'CardType1',
            status: 'Ordered',
            slotOccupancy: 1
          };
          a.createCardTemplate(cardTObj, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(cardTId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              cardTId = data.response.id;
              saveMockData('card', 'createCardTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCardTemplates', () => {
      it('should getCardTemplates', (done) => {
        try {
          a.getCardTemplates(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(cardTId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.notEqual(undefined, data.response[0].id);
                assert.notEqual(null, data.response[0].id);
                assert.notEqual('', data.response[0].id);
              }
              saveMockData('card', 'getCardTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should getCardTemplates by id', (done) => {
        try {
          a.getCardTemplates(cardTId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(cardTId, data.response.id);
              } else {
                assert.equal(cardTNm, data.response.name);
              }
              saveMockData('card', 'getCardTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCardTemplate', () => {
      it('should updateCardTemplate', (done) => {
        try {
          const ucardTObj = {
            name: `${cardTNm}.updated`,
            type: 'oci/cardtemplate'
          };
          const queryP = {
            fs: 'dynAttrs,derivedAttrs'
          };
          a.updateCardTemplate(cardTId, ucardTObj, queryP, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(cardTId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              cardTNm = ucardTObj.name;
              saveMockData('card', 'updateCardTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCardTemplates', () => {
      it('should getCardTemplates by filter', (done) => {
        try {
          const filterObj = {
            name: cardTNm
          };
          a.getCardTemplates(null, filterObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(cardTId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.equal(cardTId, data.response[0].id);
                assert.equal(cardTNm, data.response[0].name);
              }
              saveMockData('card', 'getCardTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCardTemplate', () => {
      it('should deleteCardTemplate', (done) => {
        try {
          a.deleteCardTemplate(cardTId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('card', 'deleteCardTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let portId = 1;
    let portNm = `test${Math.random()}`;

    describe('#createPort', () => {
      it('should createPort', (done) => {
        const portObj = {
          name: portNm,
          type: 'oci/port',
          status: 'OK',
          bandwidth: 'GIGE',
          connectortype: 'RJ-45',
          parentCard: [
            {
              mode: 'ADDED',
              value: {
                type: 'oci/card',
                key: {
                  type: 'oci/cardKey',
                  keyValue: cardId
                }
              }
            }
          ]
        };
        try {
          a.createPort(portObj, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(1, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual('', data.response.id);
              }
              portId = data.response.id;
              saveMockData('port', 'createPort', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPorts', () => {
      it('should getPorts', (done) => {
        try {
          a.getPorts(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(portId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.notEqual(undefined, data.response[0].id);
                assert.notEqual(null, data.response[0].id);
                assert.notEqual('', data.response[0].id);
              }
              saveMockData('port', 'getPorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should getPorts by id', (done) => {
        try {
          a.getPorts(portId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(portId, data.response.id);
              } else {
                assert.equal(portNm, data.response.name);
              }
              saveMockData('port', 'getPorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePort', () => {
      it('should updatePort', (done) => {
        try {
          const uportObj = {
            name: `${portNm}.updated`,
            type: 'oci/port',
            status: 'OK',
            bandWidth: 'GIGE',
            connectorType: 'RJ-45'
          };
          const queryP = {
            fs: 'dynAttrs,derivedAttrs'
          };
          a.updatePort(portId, uportObj, queryP, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(portId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              portNm = uportObj.name;
              saveMockData('port', 'updatePort', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPorts', () => {
      it('should getPorts by filter', (done) => {
        try {
          const filterObj = {
            name: portNm
          };
          a.getPorts(null, filterObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(portId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.equal(portNm, data.response[0].name);
              }
              saveMockData('port', 'getPorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let portTId = 1;
    let portTNm = `test${Math.random()}`;

    describe('#createPortTemplate', () => {
      it('should createPortTemplate', (done) => {
        try {
          const portTObj = {
            name: portTNm,
            type: 'oci/porttemplate',
            bandwidth: '1000BASET',
            connectortype: 'BNC',
            status: 'OK'
          };
          a.createPortTemplate(portTObj, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(portTId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual('', data.response.id);
              }
              portTId = data.response.id;
              saveMockData('port', 'createPortTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortTemplates', () => {
      it('should getPortTemplates', (done) => {
        try {
          a.getPortTemplates(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(portTId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.notEqual(undefined, data.response[0].id);
                assert.notEqual(null, data.response[0].id);
                assert.notEqual('', data.response[0].id);
              }
              saveMockData('port', 'getPortTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should getPortTemplates by id', (done) => {
        try {
          a.getPortTemplates(portTId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(portTId, data.response.id);
              } else {
                assert.equal(portTNm, data.response.name);
              }
              saveMockData('port', 'getPortTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePortTemplate', () => {
      it('should updatePortTemplate', (done) => {
        try {
          const uportTObj = {
            name: `${portTNm}.updated`,
            type: 'oci/porttemplate',
            status: 'OK',
            bandWidth: 'GIGE',
            connectorType: 'RJ-45'
          };
          const queryP = {
            fs: 'dynAttrs,derivedAttrs'
          };
          a.updatePortTemplate(portTId, uportTObj, queryP, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(portTId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              portTNm = uportTObj.name;
              saveMockData('port', 'updatePortTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortTemplates', () => {
      it('should getPortTemplates by filter', (done) => {
        try {
          const filterObj = {
            name: portTNm
          };
          a.getPortTemplates(null, filterObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(portTId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.equal(portTNm, data.response[0].name);
              }
              saveMockData('port', 'getPortTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePortTemplate', () => {
      it('should deletePortTemplate', (done) => {
        try {
          a.deletePortTemplate(portTId, null, null, (data, error) => {
            try {
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('trail', 'deletePortTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let trailId = 1;
    let trailNm = `test${Math.random()}`;

    describe('#createTrail', () => {
      it('should createTrail', (done) => {
        try {
          const trailObj = {
            name: trailNm,
            type: 'occ/trail',
            trailtype: 'SERVICE',
            status: 'PENDING',
            bandwidth: '25 MBPS',
            assignmentType: 'NONE',
            protectionType: 'NONE'
          };
          a.createTrail(trailObj, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(trailId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              trailId = data.response.id;
              saveMockData('trail', 'createTrail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrails', () => {
      it('should getTrails', (done) => {
        try {
          a.getTrails(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(trailId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.notEqual(undefined, data.response[0].id);
                assert.notEqual(null, data.response[0].id);
                assert.notEqual('', data.response[0].id);
              }
              saveMockData('trail', 'getTrails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should getTrails by id', (done) => {
        try {
          a.getTrails(trailId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(1, data.response.id);
              } else {
                assert.equal(trailNm, data.response.name);
              }
              saveMockData('trail', 'getTrails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrailQuery', () => {
      it('should getTrailQuery', (done) => {
        try {
          const filterObj = {
            name: trailNm
          };
          a.getTrailQuery(filterObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(trailId, data.response.id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              saveMockData('trail', 'getTrails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTrail', () => {
      it('should updateTrail', (done) => {
        try {
          const utrailObj = {
            name: `${trailNm}.updated`,
            type: 'occ/trail'
          };
          const queryP = {
            fs: 'dynAttrs,derivedAttrs'
          };
          a.updateTrail(trailId, utrailObj, queryP, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(trailId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              trailNm = utrailObj.name;
              saveMockData('trail', 'updateTrail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrails', () => {
      it('should getTrails by filter', (done) => {
        try {
          const filterObj = {
            name: trailNm
          };
          a.getTrails(null, filterObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(trailId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.equal(trailNm, data.response[0].name);
              }
              saveMockData('trail', 'getTrails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const siteQuery = { name: trailNm };
    describe('#updateTrailBySite', () => {
      it('should updateTrailBySite', (done) => {
        try {
          const utrailObj = {
            name: `${trailNm}.upagain`,
            type: 'occ/trail'
          };
          const queryP = {
            fs: 'dynAttrs,derivedAttrs'
          };
          a.updateTrailBySite(siteQuery, utrailObj, queryP, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(trailId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              trailNm = utrailObj.name;
              saveMockData('trail', 'updateTrailBySite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTrail', () => {
      it('should deleteTrail', (done) => {
        try {
          a.deleteTrail(trailId, null, null, (data, error) => {
            try {
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('trail', 'deleteTrail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let trailTId = 1;
    let trailTNm = `test${Math.random()}`;

    describe('#createTrailTemplate', () => {
      it('should createTrailTemplate', (done) => {
        try {
          const trailTObj = {
            name: trailTNm,
            type: 'occ/trailtemplate',
            trailTemplateType: 'SERVICE',
            status: 'PENDING',
            bandwidth: '25 MBPS',
            assignmentType: 'NONE',
            protectionType: 'NONE'
          };
          a.createTrailTemplate(trailTObj, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(trailTId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              trailTId = data.response.id;
              saveMockData('trail', 'createTrailTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrailTemplates', () => {
      it('should getTrailTemplates', (done) => {
        try {
          a.getTrailTemplates(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(trailTId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.notEqual(undefined, data.response[0].id);
                assert.notEqual(null, data.response[0].id);
                assert.notEqual('', data.response[0].id);
              }
              saveMockData('trail', 'getTrailTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should getTrailTemplates by id', (done) => {
        try {
          a.getTrailTemplates(trailTId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(1, data.response.id);
              } else {
                assert.equal(trailTNm, data.response.name);
              }
              saveMockData('trail', 'getTrailTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTrailTemplate', () => {
      it('should updateTrailTemplate', (done) => {
        try {
          const utrailTObj = {
            name: `${trailTNm}.updated`,
            type: 'occ/trailtemplate'
          };
          const queryP = {
            fs: 'dynAttrs,derivedAttrs'
          };
          a.updateTrailTemplate(trailTId, utrailTObj, queryP, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(trailTId, data.response.id);
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              trailTNm = utrailTObj.name;
              saveMockData('trail', 'updateTrailTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrailTemplates', () => {
      it('should getTrailTemplates by filter', (done) => {
        try {
          const filterTObj = {
            name: trailTNm
          };
          a.getTrailTemplates(null, filterTObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(trailTId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.equal(trailTNm, data.response[0].name);
              }
              saveMockData('trail', 'getTrailTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTrailTemplate', () => {
      it('should deleteTrailTemplate', (done) => {
        try {
          a.deleteTrailTemplate(trailTId, null, null, (data, error) => {
            try {
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('trail', 'deleteTrailTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const equipmentId = 1;
    const equipmentNm = '001.02';

    describe('#getEquipments', () => {
      it('should getEquipments', (done) => {
        try {
          a.getEquipments(null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(equipmentId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.notEqual(undefined, data.response[0].id);
                assert.notEqual(null, data.response[0].id);
                assert.notEqual('', data.response[0].id);
              }
              saveMockData('equipment', 'getEquipments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should getEquipments by filter', (done) => {
        try {
          const filterObj = {
            name: equipmentNm
          };
          a.getEquipments(filterObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(equipmentId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.equal(equipmentNm, data.response[0].name);
              }
              saveMockData('equipment', 'getEquipments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePort', () => {
      it('should deletePort', (done) => {
        try {
          a.deletePort(portId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('port', 'deletePort', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCard', () => {
      it('should deleteCard', (done) => {
        try {
          a.deleteCard(cardId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('card', 'deleteCard', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSlot', () => {
      it('should deleteSlot', (done) => {
        try {
          a.deleteSlot(slotId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('slot', 'deleteSlot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteContainer', () => {
      it('should deleteContainer', (done) => {
        try {
          a.deleteContainer(containerId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('container', 'deleteContainer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteShelf', () => {
      it('should deleteShelf', (done) => {
        try {
          a.deleteShelf(shelfId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('shelf', 'deleteShelf', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSite', () => {
      it('should deleteSite', (done) => {
        try {
          a.deleteSite(siteId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('site', 'deleteSite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let genericId = 1;
    let genericNm = `testGen${Math.random()}`;

    describe('#createGeneric', () => {
      it('should createGeneric', (done) => {
        try {
          const genObj = {
            name: genericNm,
            type: 'oci/site',
            _type: 'OTHER',
            status: 'Ordered',
            returnFull: true
          };
          a.createGeneric('/oss-core-ws/rest/oci/site', genObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(genericId, data.response.id);
                genericNm = data.response.name;
              } else {
                assert.notEqual(undefined, data.response.id);
                assert.notEqual(null, data.response.id);
                assert.notEqual('', data.response.id);
              }
              genericId = data.response.id;
              saveMockData('generic', 'createGeneric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGenerics', () => {
      it('should getGenerics', (done) => {
        try {
          const qParams = { returnFull: true };
          a.getGenerics('/oss-core-ws/rest/oci/site', null, qParams, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(genericId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.notEqual(undefined, data.response[0].id);
                assert.notEqual(null, data.response[0].id);
                assert.notEqual('', data.response[0].id);
              }
              saveMockData('generic', 'getGenerics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should getGenerics by id', (done) => {
        try {
          const qParams = { returnFull: true };
          a.getGenerics('/oss-core-ws/rest/oci/site', genericId, qParams, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(genericId, data.response[0].id);
              } else {
                assert.equal(genericNm, data.response[0].name);
              }
              saveMockData('generic', 'getGenerics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGeneric', () => {
      it('should updateGeneric', (done) => {
        try {
          const ugenObj = {
            name: `${genericNm}.updated`,
            type: 'oci/site'
          };
          a.updateGeneric('/oss-core-ws/rest/oci/site', genericId, ugenObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              genericNm = ugenObj.name;
              saveMockData('generic', 'updateGeneric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGenerics', () => {
      it('should getGenerics by filter', (done) => {
        try {
          const filterObj = {
            name: genericNm,
            returnFull: true
          };
          a.getGenerics('/oss-core-ws/rest/oci/site', null, filterObj, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(genericId, data.response[0].id);
              } else {
                assert.notEqual(0, data.response.length);
                assert.equal(genericId, data.response[0].id);
                assert.equal(genericNm, data.response[0].name);
              }
              saveMockData('generic', 'getGenerics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGeneric', () => {
      it('should deleteGeneric', (done) => {
        try {
          a.deleteGeneric('/oss-core-ws/rest/oci/site', genericId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success', data.response);
              } else {
                assert.equal('success', data.response);
              }
              saveMockData('generic', 'deleteGeneric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const addlH = {
      interactive: false
    };
    let prsiteId = 1;
    let prsiteNm = `prtest${Math.random()}`;
    describe('#processRequest', () => {
      it('should create site via processRequest', (done) => {
        try {
          const siteObj = {
            name: prsiteNm,
            sitetype: 'OTHER',
            status: 'Ordered'
          };
          const queryP = {
            fs: 'dynAttrs,derivedAttrs'
          };
          a.processRequest('site', 'create', queryP, siteObj, addlH, (data, error) => {
            try {
              // runCommonAsserts(data, error);
              if (stub) {
                assert.equal(prsiteId, data.id);
                prsiteNm = data.name;
              } else {
                assert.notEqual(undefined, data.id);
                assert.notEqual(null, data.id);
                assert.notEqual('', data.id);
              }
              prsiteId = data.id;
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get site via processRequest - query', (done) => {
        try {
          const queryP = {
            fs: 'dynAttrs,derivedAttrs',
            name: prsiteNm
          };
          a.processRequest('site', 'query', queryP, null, addlH, (data, error) => {
            try {
              // runCommonAsserts(data, error);
              if (stub) {
                assert.equal(prsiteId, data[0].id);
              } else {
                assert.notEqual(undefined, data[0].id);
                assert.notEqual(null, data[0].id);
                assert.notEqual('', data[0].id);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should update site via processRequest', (done) => {
        try {
          const usiteObj = {
            name: `${prsiteNm}.updated`
          };
          const queryP = {
            fs: 'dynAttrs,derivedAttrs',
            key: prsiteId
          };
          a.processRequest('site', 'update', queryP, usiteObj, addlH, (data, error) => {
            try {
              // runCommonAsserts(data, error);
              if (stub) {
                assert.equal(prsiteId, data.id);
              } else {
                assert.notEqual(undefined, data.id);
                assert.notEqual(null, data.id);
                assert.notEqual('', data.id);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get site via processRequest - fetchByKey', (done) => {
        try {
          const queryP = {
            fs: 'dynAttrs,derivedAttrs',
            key: prsiteId
          };
          a.processRequest('site', 'fetchByKey', queryP, null, addlH, (data, error) => {
            try {
              // runCommonAsserts(data, error);
              if (stub) {
                assert.equal(prsiteId, data.id);
              } else {
                assert.notEqual(undefined, data.id);
                assert.notEqual(null, data.id);
                assert.notEqual('', data.id);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should delete site via processRequest', (done) => {
        try {
          const queryP = {
            fs: 'dynAttrs,derivedAttrs',
            key: prsiteId
          };
          a.processRequest('site', 'delete', queryP, null, addlH, (data, error) => {
            try {
              // runCommonAsserts(data, error);
              if (stub) {
                assert.equal('success deleting', data);
              } else {
                assert.equal('success deleting', data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
