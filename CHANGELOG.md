
## 3.4.4 [10-15-2024]

* Changes made at 2024.10.14_21:23PM

See merge request itentialopensource/adapters/adapter-eai!24

---

## 3.4.3 [08-25-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-eai!22

---

## 3.4.2 [08-14-2024]

* Changes made at 2024.08.14_19:41PM

See merge request itentialopensource/adapters/adapter-eai!21

---

## 3.4.1 [08-07-2024]

* Changes made at 2024.08.06_21:45PM

See merge request itentialopensource/adapters/adapter-eai!20

---

## 3.4.0 [05-14-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/inventory/adapter-eai!19

---

## 3.3.4 [03-26-2024]

* Changes made at 2024.03.26_14:48PM

See merge request itentialopensource/adapters/inventory/adapter-eai!18

---

## 3.3.3 [03-13-2024]

* Changes made at 2024.03.13_14:04PM

See merge request itentialopensource/adapters/inventory/adapter-eai!17

---

## 3.3.2 [03-11-2024]

* Changes made at 2024.03.11_14:14PM

See merge request itentialopensource/adapters/inventory/adapter-eai!16

---

## 3.3.1 [02-26-2024]

* Changes made at 2024.02.26_13:42PM

See merge request itentialopensource/adapters/inventory/adapter-eai!15

---

## 3.3.0 [12-29-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/inventory/adapter-eai!14

---

## 3.2.0 [05-20-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/inventory/adapter-eai!13

---

## 3.1.8 [03-13-2022]

- Security vulnerabilities and change mockdata
- Migration to the latest foundation and broker ready
  - Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
  - Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
  - Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
  - Add scripts for easier authentication, removing hooks, etc
  - Script changes (install script as well as database changes in other scripts)
  - Double # of path vars on generic call
  - Update versions of foundation (e.g. adapter-utils)
  - Update npm publish so it supports https
  - Update dependencies
  - Add preinstall for minimist
  - Fix new lint issues that came from eslint dependency change
  - Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
  - Add the adapter type in the package.json
  - Add AdapterInfo.js script
  - Add json-query dependency
  - Add the propertiesDecorators.json for product encryption
  - Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
  - Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
  - Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
  - Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
  - Add AdapterInfo.json
  - Add systemName for documentation

See merge request itentialopensource/adapters/inventory/adapter-eai!12

---

## 3.1.7 [03-04-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/inventory/adapter-eai!11

---

## 3.1.6 [07-07-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/inventory/adapter-eai!10

---

## 3.1.5 [04-20-2020]

- Fixes the types in the pronghorn.json so they are ajv compliant

See merge request itentialopensource/adapters/inventory/adapter-eai!9

---

## 3.1.4 [03-26-2020]

- Changes made to processRequest so that the adapter would be non breaking.
Also added back in generic calls.

See merge request itentialopensource/adapters/inventory/adapter-eai!8

---

## 3.1.3 [01-30-2020]

- Fixed the processRequest -> createSlot which was setting the type to oci/site instead of oci/slot.
Tested with Customer

See merge request itentialopensource/adapters/inventory/adapter-eai!7

---

## 3.1.2 [01-09-2020]

- Bring the adapter up to the latest foundation

See merge request itentialopensource/adapters/inventory/adapter-eai!6

---

## 3.1.1 [11-21-2019]

- Fix entitypaths and move headers into the global request properties.

See merge request itentialopensource/adapters/inventory/adapter-eai!5

---

## 3.1.0 [11-07-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/inventory/adapter-eai!4

---

## 3.0.0 [09-19-2019] & 2.0.0 [08-16-2019]

- Create the major branch since the open sourced adapter could have breaking issues with the pre-existing adapter that was not open sourced

See merge request itentialopensource/adapters/staging/adapter-eai!3

---
